from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters

from bot.constants import CONVERSATIONS
from bot.db import get_msg_guita
from bot.utils import restricted
from .cancel import cancel

GET_TEXT = CONVERSATIONS.create_consts(1)

def create_handler():
    # https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/conversationbot.py
    # Add conversation handler
    return ConversationHandler(
        entry_points=[CommandHandler('mensaje_directo', entry_point)],
        states={
            GET_TEXT: [MessageHandler((Filters.text & ~Filters.command), get_text)],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

@restricted
def entry_point(update, context):
    update.message.reply_text(f'⏺ Okey, mandame el texto del mensaje que querés que envíe (o cancelá con /cancelar). ' \
    'Tené en cuenta que lo voy a mandar ni bien lo reciba.')

    return GET_TEXT

@restricted
def get_text(update, context):
    # si usamos text_markdown_v2 a secas no se arman previews de los links
    # text_markdown_v2 - https://python-telegram-bot.readthedocs.io/en/stable/telegram.message.html#telegram.Message.text_markdown_v2
    text = update.message.text_markdown_v2_urled

    msg = get_msg_guita()
    context.bot.send_message(chat_id=msg.grupo, text=text, parse_mode='MarkdownV2')

    update.message.reply_text('✅ Enviado!')

    return ConversationHandler.END
