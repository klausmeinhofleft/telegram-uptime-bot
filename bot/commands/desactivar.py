from telegram.ext import CommandHandler

from bot.db import get_msg_guita
from bot.job import clean_msg_job
from bot.utils import restricted
from .cancel import cancel

def create_handler():
    return CommandHandler("desactivar", command)

@restricted
def command(update, context):
    msg = get_msg_guita()

    # validaciones
    if not msg.activo:
        update.message.reply_text('❇️ Ya estoy inactivo')
        return

    # updateamos DB
    msg.activo = False
    msg.save()

    # quitamos job si está andando
    clean_msg_job(context)

    # devolvemos
    update.message.reply_text('✅ Desactivado')
